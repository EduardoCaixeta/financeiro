package view.panels;

import java.awt.Color;

import javax.swing.BorderFactory;

import model.entity.Users;
import conexoes.GERAIS;

public class FrameLogin extends GERAIS {

	PanelLogin login = new PanelLogin();

	public FrameLogin() {
		super();
		login.getButtonEntrar().addActionListener(e -> entrar_Click());
	}

	@SuppressWarnings({ "deprecation", "static-access" })
	public void entrar_Click() {
		try {
			if (login.getTxtUsername().getText().isEmpty() == false) {
				login.getTxtUsername().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
				if (login.getTxtPassword().getText().isEmpty() == false) {
					login.getTxtPassword().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
					Users user = new Users();
					user.setPassword(login.getTxtPassword().getText());
					user.setUsername(login.getTxtUsername().getText());
					if (super.userController.login(user) != -1) {
						super.login();
					}
					else
						throw new Exception("Username e/ou senha incorreto(s).");
				} else
				{
					login.getTxtPassword().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
					throw new Exception("Insira a senha.");
				}
			} else {
				login.getTxtUsername().setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.RED));
				throw new Exception("Insira o username.");
			}
		} catch (Exception e) {
			super.erro(e.getMessage());
		}
	}
}
