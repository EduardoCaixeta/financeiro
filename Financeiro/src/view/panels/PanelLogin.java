package view.panels;

import java.awt.Color;
import javax.swing.*;

@SuppressWarnings("serial")
public class PanelLogin extends JPanel {

    /**
     * Creates new form PanelLogin
     */
    public PanelLogin() {
        initComponents();
        
    }
                 
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        labelUsername = new JLabel();
        txtUsername = new JTextField();
        labelPassword = new JLabel();
        txtPassword = new JPasswordField();
        buttonEntrar = new JButton();
        buttonCadastrar = new JButton();
        buttonEMS = new JButton();


        setLayout(new java.awt.GridBagLayout());

        labelUsername.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelUsername.setText("Username:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(19, 19, 0, 0);
        add(labelUsername, gridBagConstraints);

        txtUsername.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtUsername.setOpaque(false);
        txtUsername.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 147;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 5, 0, 33);
        add(txtUsername, gridBagConstraints);

        labelPassword.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        labelPassword.setText("Senha:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(21, 41, 0, 0);
        add(labelPassword, gridBagConstraints);

        txtPassword.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtPassword.setOpaque(false);
        txtPassword.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 6;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 148;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(18, 4, 0, 33);
        add(txtPassword, gridBagConstraints);

        buttonEntrar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonEntrar.setText("Entrar");
        buttonEntrar.setFocusPainted(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(18, 16, 0, 0);
        add(buttonEntrar, gridBagConstraints);

        buttonCadastrar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonCadastrar.setText("Cadastrar");
        buttonCadastrar.setFocusPainted(false);
     
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(7, 4, 0, 0);
        add(buttonCadastrar, gridBagConstraints);

        buttonEMS.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        buttonEMS.setText("Esqueci minha senha");
        buttonEMS.setFocusPainted(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(7, 57, 17, 0);
        add(buttonEMS, gridBagConstraints);
    }                                             


    // Variables declaration - do not modify                     
    private JButton buttonCadastrar;
    private JButton buttonEMS;
    private JButton buttonEntrar;
    private JLabel labelPassword;
    private JLabel labelUsername;
    private JPasswordField txtPassword;
    private JTextField txtUsername;
	public JButton getButtonCadastrar() {
		return buttonCadastrar;
	}

	public void setButtonCadastrar(JButton buttonCadastrar) {
		this.buttonCadastrar = buttonCadastrar;
	}

	public JButton getButtonEMS() {
		return buttonEMS;
	}

	public void setButtonEMS(JButton buttonEMS) {
		this.buttonEMS = buttonEMS;
	}

	public JButton getButtonEntrar() {
		return buttonEntrar;
	}

	public void setButtonEntrar(JButton buttonEntrar) {
		this.buttonEntrar = buttonEntrar;
	}

	public JLabel getLabelPassword() {
		return labelPassword;
	}

	public void setLabelPassword(JLabel labelPassword) {
		this.labelPassword = labelPassword;
	}

	public JLabel getLabelUsername() {
		return labelUsername;
	}

	public void setLabelUsername(JLabel labelUsername) {
		this.labelUsername = labelUsername;
	}

	public JPasswordField getTxtPassword() {
		return txtPassword;
	}

	public void setTxtPassword(JPasswordField txtPassword) {
		this.txtPassword = txtPassword;
	}

	public JTextField getTxtUsername() {
		return txtUsername;
	}

	public void setTxtUsername(JTextField txtUsername) {
		this.txtUsername = txtUsername;
	}
  
}