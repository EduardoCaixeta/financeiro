package model.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable (tableName = "transações")
public class Transacoes {
	@DatabaseField(generatedId = true)
	int id;

	@DatabaseField
	boolean debito;

	@DatabaseField
	double valor;

	@DatabaseField(dataType=DataType.DATE)
    public Date date; 
	
	@DatabaseField
	String descricao;
	
	public String printDate() {
        return new SimpleDateFormat("dd/MM/yyyy").format(date.getTime());
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isDebito() {
		return debito;
	}

	public void setDebito(boolean debito) {
		this.debito = debito;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	
}
