package model.entity;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable (tableName = "users")
public class Users {
	@DatabaseField(generatedId = true)
	int id;

	@DatabaseField
	String username;

	@DatabaseField
	String password;

	@DatabaseField
	String email;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		String emailUP = "";
		for (int i = 0; i < email.length(); i++) {
			try {
				emailUP += Character.toUpperCase(email.charAt(i));
			} catch (Exception e) {
				emailUP += email.charAt(i);
			}
		}
		return emailUP;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
