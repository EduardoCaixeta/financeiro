package model;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;

import model.entity.Users;

import java.util.ArrayList;
import java.util.List;

public class UsersDao {
	public static Database database;
	public static Dao<Users, Integer> dao;
	private List<Users> listaUsers;

	public UsersDao(Database database) throws Exception {
		UsersDao.setDatabase(database);
		listaUsers = new ArrayList<Users>();
	}

	public static void setDatabase(Database database) throws Exception {
		UsersDao.database = database;
		try {
			dao = DaoManager.createDao(database.getConnection(), Users.class);
			TableUtils.createTableIfNotExists(database.getConnection(), Users.class);
		} catch (SQLException e) {
			throw new Exception("N�o foi possvel ler os usu�rios.");
		}
	}

	public void create(Users user) throws Exception {
			int nrows = 0;
			nrows = dao.create(user);
			if (nrows == 0)
				throw new Exception("Erro ao cadastrar o usu�rio.");
			user.setId(listaUsers.size());
			listaUsers.add(user);
	
	}

	public boolean existUsers(Users user) throws Exception {
		try {
			String name = user.getUsername();
			for (int i = 0; i < listaUsers.size(); i++) {
				if (name.equals(listaUsers.get(i).getUsername())
						&& user.getPassword().equals(listaUsers.get(i).getPassword())) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			throw new Exception("N�o foi possvel verificar o usu�rio.");
		}
	}

	public Users getUser(String dado, String tipo) throws Exception
	{
		if(tipo.equals("username"))
		{
			try {
				String name = dado;
				int id = -1;
				for (int i = 0; i < listaUsers.size(); i++) {
					if (name.equals(listaUsers.get(i).getUsername())) {
						id = i;
						break;
					}
				}
				if (id != -1)
				{
					Users u = dao.queryForId(id+1);
					return u;
				}
				else 
					throw new Exception("~");
			} catch (Exception e) {
				if(e.getMessage().equals("~")) throw new Exception("N�o h� cadastros com este username.");
				else throw new Exception("N�o foi poss�vel verificar o username.");
			}
		}
		else if( tipo.equals("email"))
		{
			try {
				
				int id = -1;
				for (int i = 0; i < listaUsers.size(); i++) {
					if (dado.equals(listaUsers.get(i).getEmail())) {
						id = i;
						break;
					}
				}
				if (id != -1)
				{
					Users u = dao.queryForId(id+1);
					return u;
				}
				else 
					throw new Exception("~");
			
			} catch (Exception e) {
				if(e.getMessage().equals("~")) throw new Exception("N�o h� cadastros com este email.");
				else throw new Exception("N�o foi poss�vel verificar o email.");
			}
		}
		else throw new Exception("Tipo de dado inv�lido.");
	}
	
	public boolean existUsername(String name) throws Exception {
		try {
		
			for (int i = 0; i < listaUsers.size(); i++) {
				if (name.equals(listaUsers.get(i).getUsername())) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			throw new Exception("N�o foi possvel verificar o username.");
		}
	}

	public void update(Users user) throws Exception {
		try {
			dao.update(user);
			listaUsers.set(user.getId(), user);
		} catch (Exception e) {
			throw new Exception("N�o foi poss�vel atualizar o cadastro.");
		}
	}

	public boolean existEmail(String email) throws Exception {
		try {
			
			for (int i = 0; i < listaUsers.size(); i++) {
				if (email.equals(listaUsers.get(i).getEmail())) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			throw new Exception("N�o foi poss�vel verificar o email.");
		}

	}
	
	public int login (Users user) throws Exception
	{
		for (int i = 0; i < listaUsers.size(); i++) {
			if (user.getUsername().toUpperCase().equals(listaUsers.get(i).getUsername().toUpperCase())) {
				if(user.getPassword().equals(listaUsers.get(i).getPassword()))
				return user.getId();
			}
		}
		return -1;
	}


	public int registros() throws Exception {
		try {
			listaUsers = dao.queryForAll();
			return listaUsers.size();
		} catch (Exception e) {
			throw new Exception("N�o foi poss�vel ler os registros.");
		}
	}
}
