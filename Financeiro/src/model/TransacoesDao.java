package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;

import model.entity.Transacoes;

import java.util.ArrayList;
import java.util.List;

public class TransacoesDao {
	public static Database database;
	public static Dao<Transacoes, Integer> dao;
	private List<Transacoes> listaAll;

	public TransacoesDao(Database database) throws Exception {
		TransacoesDao.setDatabase(database);
		listaAll = new ArrayList<Transacoes>();
	}

	public static void setDatabase(Database database) throws Exception {
		TransacoesDao.database = database;
		try {
			dao = DaoManager.createDao(database.getConnection(), Transacoes.class);
			TableUtils.createTableIfNotExists(database.getConnection(), Transacoes.class);
		} catch (SQLException e) {
			throw new Exception("N�o foi possvel ler os usu�rios.");
		}
	}

	public void create(Transacoes trans) throws Exception {
		int nrows = 0;
		nrows = dao.create(trans);
		if (nrows == 0)
			throw new Exception("Erro ao cadastrar a transa��o.");
		trans.setId(listaAll.size()+1);
		listaAll.add(trans);
	}
	
	public void update(Transacoes trans) throws Exception {
		int nrows = 0;
		nrows = dao.update(trans);
		if (nrows == 0)
			throw new Exception("Erro ao atualizar a transa��o.");	
		listaAll.set(trans.getId(),trans);
	}
	
	public void delete(Transacoes trans) throws Exception {
		int nrows = 0;
		nrows = dao.delete(trans);
		if (nrows == 0)
			throw new Exception("Erro ao apagar a transa��o.");	
		listaAll.remove(trans.getId());
	}
	
	
}

