package controller;
import model.entity.Users;
import model.UsersDao;
import model.Database;

public class UsersController {
	private UsersDao dao;
	private Database  db;
	public UsersController(Database database) throws Exception {
		this.dao = new UsersDao(database);
		this.db = database;
	}

	public Database getDatabase()
	{
		return db;
	}
	
	public void create(Users username) throws Exception {
		dao.create(username);
	}

	public void update(Users username) throws Exception {
		dao.update(username);
	}

	public boolean existUsers(Users user) throws Exception {
		return dao.existUsers(user);
	}
	
	public boolean existUsername(String username) throws Exception {
		return dao.existUsername(username);
	}
	
	public Users getUser(String dado, String tipo) throws Exception {
		return dao.getUser(dado, tipo);
	}

	public int qntRegistros() throws Exception {
		return dao.registros();
	}

	public boolean existEmail(String email) throws Exception {
		return dao.existEmail(email);
	}
	
	public int login(Users user) throws Exception
	{
		try
		{
			return dao.login(user);
		}
		catch(Exception e)
		{
			throw new Exception("N�o foi poss�vel realizar o login.");
		}
	}
}
