package controller;
import model.entity.Transacoes;
import model.TransacoesDao;
import model.Database;

public class TransController {
	private TransacoesDao dao;
	private Database  db;
	public TransController(Database database) throws Exception {
		this.dao = new TransacoesDao(database);
		this.db = database;
	}

	public Database getDatabase()
	{
		return db;
	}
	
	public void create(Transacoes trans) throws Exception {
		dao.create(trans);
	}

	public void update(Transacoes trans) throws Exception {
		dao.update(trans);
	}

	public void delete(Transacoes trans) throws Exception {
		dao.delete(trans);
	}

}